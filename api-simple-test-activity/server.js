const express = require('express');
const app = express();
const PORT = 5001;

app.use(express.json());

require('./routes/userRoutes')(app, {}); //required userRoutes and send the app and empty object as its argument.
require('./routes/productRoutes')(app, {});

app.listen(PORT, ()=> {
	console.log(`Listening to PORT ${PORT}`);
})