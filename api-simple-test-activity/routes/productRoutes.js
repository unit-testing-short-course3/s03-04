module.exports = (app) => {
	app.get('/products', (req, res)=>{
		return res.status(200).send({'Message': 'Status Ok!'})
	})

	app.post('/add-product', (req, res)=>{

		/*
			Data Structure - req.body

			{
				productName: "Sunsilk",
				productPrice: 15.00,
				productDesc: "Shampoo and Conditioner"
				quantity: 5
			}
		*/
		if(!req.body.hasOwnProperty('productName')){
			return res.status(400).send({"Error": "Bad request - missing productName parameter"})
		}

		if(!req.body.hasOwnProperty('productPrice')){
			return res.status(400).send({"Error": "Bad request - missing productPrice parameter"})
		}

		if(!req.body.hasOwnProperty('productDesc')){
			return res.status(400).send({"Error": "Bad request - missing productDesc parameter"})
		}

		if(!req.body.hasOwnProperty('productDesc')){
			return res.status(400).send({"Error": "Bad request - missing productDesc parameter"})
		}

		if(!req.body.hasOwnProperty('quantity')){
			return res.status(400).send({"Error": "Bad request - missing quantity parameter"})
		}

		//return this if there are no errors encoutered
		return res.status(200).send({'Message': 'product created!'})
	})

}